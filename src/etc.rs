use ::types::*;

fn get_pos(pos: (usize, usize), dir: MoveDirection) -> (usize, usize) {
    match dir {
        MoveDirection::U => (pos.0 - 1, pos.1),
        MoveDirection::D => (pos.0 + 1, pos.1),
        MoveDirection::L => (pos.0, pos.1 - 1),
        MoveDirection::R => (pos.0, pos.1 + 1),
    }
}

pub fn generate_puzzle(start: &Puzzle, dir: MoveDirection, size: (usize, usize)) -> Option<Puzzle> {
    let start_open_pos = start.open_pos;
    let new_open_pos = get_pos(start_open_pos, dir);
    if new_open_pos.0 < 0 || new_open_pos.0 >= size.0 {
        None
    } else if new_open_pos.1 < 0 || new_open_pos.1 >= size.1 {
        None
    } else {
        let mut new_puzzle = start.clone();
        let tmp = new_puzzle.pieces[start_open_pos.0][start_open_pos.1];
        new_puzzle.pieces[start_open_pos.0][start_open_pos.1] =
            new_puzzle.pieces[new_open_pos.0][new_open_pos.1];
        new_puzzle.pieces[new_open_pos.0][new_open_pos.1] = tmp;
        new_puzzle.open_pos = new_open_pos;
        Some(new_puzzle)
    }
}

pub fn print_puzzle(puzzle: Puzzle) {
    for row in puzzle.pieces {
        for col in row {
            print!("{}", col);
        }
        println!("");
    }
}
