use std::cmp::Ordering;
use std::fmt;

#[derive(Eq, PartialEq, Clone)]
pub enum MoveDirection {
    L,
    R,
    U,
    D,
}

impl fmt::Display for MoveDirection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            MoveDirection::L => write!(f, "L"),
            MoveDirection::R => write!(f, "R"),
            MoveDirection::U => write!(f, "U"),
            MoveDirection::D => write!(f, "D"),
        }
    }
}

#[derive(Eq, PartialEq, Clone)]
pub struct Puzzle {
    pub pieces: Vec<Vec<char>>,
    pub open_pos: (usize, usize),
}

#[derive(Eq, PartialEq)]
pub struct PuzzleState {
    pub f_val: usize,
    pub puzzle: Puzzle,
    pub moves: Vec<MoveDirection>,
}
impl Ord for PuzzleState {
    fn cmp(&self, other: &PuzzleState) -> Ordering {
        other.f_val.cmp(&self.f_val)
    }
}
impl PartialOrd for PuzzleState {
    fn partial_cmp(&self, other: &PuzzleState) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
