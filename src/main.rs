use etc::*;
use std::cmp::{max, min};
use std::collections::BinaryHeap;
use std::io;
use std::io::prelude::*;
use types::*;

mod etc;
mod types;

const move_dirs: [MoveDirection; 4] = [
    MoveDirection::L,
    MoveDirection::R,
    MoveDirection::U,
    MoveDirection::D,
];

fn main() {
    let mut input: Vec<Vec<char>> = Vec::new();
    let stdin = io::stdin();
    let mut n_tmp = None;
    for line in stdin.lock().lines() {
        let line = line.unwrap();
        let mut chars: Vec<char> = Vec::new();
        for c in line.chars() {
            chars.push(c);
        }
        if let Some(n_tmp) = n_tmp {
            assert_eq!(n_tmp, chars.len());
        } else {
            n_tmp = Some(chars.len());
        }

        input.push(chars);
    }

    println!("\n------------\n");

    let length = input.len();
    assert_eq!(length % 2, 0);

    let m = length / 2;
    let n = n_tmp.unwrap();

    let target_pieces = input.split_off(m);
    let start_pieces = input;

    let start = Puzzle {
        pieces: start_pieces.clone(),
        open_pos: (0, 0),
    };
    let target = Puzzle {
        pieces: target_pieces.clone(),
        open_pos: (0, 0),
    };

    println!("Starting layout:");
    print_puzzle(start);

    println!("\n------------\n");

    println!("Target layout:");
    print_puzzle(target);

    let mut open: BinaryHeap<PuzzleState> = BinaryHeap::new();
    open.push(PuzzleState {
        puzzle: start,
        moves: Vec::new(),
        f_val: 0,
    });

    let mut closed: Vec<PuzzleState> = Vec::new();

    let mut final_state: PuzzleState;
    while open.len() > 0 {
        let next = open.pop().unwrap();

        let mut children: Vec<PuzzleState> = Vec::new();

        for dir in move_dirs.iter() {
            match generate_puzzle(&next.puzzle, *dir, (m, n)) {
                Some(p) => {
                    let mut next_moves = next.moves.clone();
                    next_moves.push(*dir);
                    children.push(PuzzleState {
                        f_val: 0,
                        puzzle: p,
                        moves: next_moves,
                    });
                }
                _ => (),
            }
        }

        let mut found = false;
        for child in children {
            if child.puzzle == target {
                final_state = child;
                found = true;
                break;
            }
            child.f_val = f(&child, &target, &(m, n));

            let mut should_add = true;
            for node in open {
                if child.puzzle == node.puzzle && child.f_val > node.f_val {
                    should_add = false;
                    break;
                }
            }
            for node in closed {
                if child.puzzle == node.puzzle && child.f_val > node.f_val {
                    should_add = false;
                    break;
                }
            }

            if should_add {
                open.push(child);
            }
        }

        if found {
            break;
        }

        closed.push(next);
    }

    println!("Found answer: ");
    for slide in final_state.moves {
        println!("{}", slide);
    }
}

fn f(state: &PuzzleState, target: &Puzzle, size: &(usize, usize)) -> usize {
    return g(state) + h(state, target, size);
}

fn g(state: &PuzzleState) -> usize {
    return state.moves.len();
}

fn h(state: &PuzzleState, target: &Puzzle, size: &(usize, usize)) -> usize {
    let mut dist = 0;
    for i in 0..size.0 {
        for j in 0..size.1 {
            let target_pos = get_target_pos(state.puzzle.pieces[i][j], target).unwrap();
            let max_m = max(target_pos.0, i);
            let min_m = min(target_pos.0, i);
            let max_n = max(target_pos.1, j);
            let min_n = min(target_pos.1, j);
            dist += (max_m - min_m) + (max_n - min_n);
        }
    }

    return dist;
}

fn get_target_pos(search: char, target: &Puzzle) -> Option<(usize, usize)> {
    let mut colIndex = 0;
    let mut rowIndex = 0;
    for row in target.pieces {
        for piece in row {
            if search == piece {
                return Some((rowIndex, colIndex));
            }
            colIndex += 1;
        }
        rowIndex += 1;
    }

    return None;
}
